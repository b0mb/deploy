class ArticlesController < ApplicationController
	include ArticlesHelper

	before_filter :require_login, only: [:new, :create, :edit, :update, :destroy, :preview]
	before_action :load_article, only: [:show, :destroy, :edit, :update]

	def index
    if params[:search].blank?
    	@articles = Article.paginate(:page => params[:page], :per_page => 5)
    else
    	@articles = Article.solr_search do #conflict with active_admin ransack
    		fulltext params[:search]
    		paginate :page => params[:page], :per_page => 5
    	end.results
    end
		@tags = Tag.all
	end

	def show
		@commentable = @article
		@comments = @commentable.comments
		@comment = Comment.new
	end

	def new
		@article = Article.new
	end

	def create
		@article = Article.create(article_params)
		if @article.save
		  redirect_to article_path(@article)
		else
		  render 'new'
		end
	end

	def destroy
		@article.destroy
		redirect_to articles_path
	end

	def edit
	end

	def update
		@article.update(article_params)

		flash.notice = "Article '#{@article.title}' updated"

		redirect_to article_path(@article)
	end

	def preview
		@content = params[:content]
		respond_to do |format|
		  format.js
		end
	end

	def archive
		@articles = Article.all
		@article_months = @articles.group_by { |t| t.created_at.beginning_of_month }
		@paginate_articles = Article.paginate(:page => params[:page], :per_page => 40)
	end

  def about
  end

	private
	  def load_article
	  	@article = Article.friendly.find(params[:id])
	  end

	  def not_authenticated
      flash.alert = "Login require"
	  	redirect_to login_path
	  end

end
