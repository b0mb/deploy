$(function(){
  $('.post_body').jTruncate({
      length: 4000,
      minTrail: 0,
      moreText: "[查看全部]",
      lessText: "[隐藏部分]",
      ellipsisText: "........",
      moreAni: "fast",
      lessAni: 2000
  });
});
