module ArticlesHelper

	def article_params
		params.require(:article).permit(:title, :body, :tag_list, :image)
	end

  def tag_class(name)
    #Rails.cache.write('name',["blue","green",""].sample)
    Rails.cache.fetch("#{name}") {["blue","green",""].sample}
  end

end
