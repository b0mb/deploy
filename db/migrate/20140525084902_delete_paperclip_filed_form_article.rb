class DeletePaperclipFiledFormArticle < ActiveRecord::Migration
  def self.up
  	remove_column :articles, :image_file_name
  	remove_column :articles, :image_content_type
  	remove_column :articles, :image_file_size
  	remove_column :articles, :image_updated_at
  end

  def self.down
  	add_column :articles, :image_file_name, :string
  	add_column :articles, :image_content_type, :string
  	add_column :articles, :image_file_size, :string
  end
end
